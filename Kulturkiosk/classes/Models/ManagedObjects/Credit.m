#import "Credit.h"
#import "KKLanguage.h"

@implementation Credit

// Custom logic goes here.

-(NSString*) localizedCredit
{
  if(self.credit_type && [self.credit_type length] && self.name && [self.name length])
    return [NSString stringWithFormat:@"%@: %@", [KKLanguage localizedString:self.credit_type], self.name];
  return nil;
}

@end
