#import "Device.h"


@interface Device ()

// Private interface goes here.

@end


@implementation Device

// Custom logic goes here.

- (NSArray*)files
{
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"resource.device == %@", self];
  
  return [Files objectsWithPredicate:pred];
}


- (UIImage*)idleScreenBackground
{
  UIImage *background = nil;
  
  NSString *strFormat = [Nodes stringForImageFormat:KKFormatOriginal];
  
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"resource.device == %@ AND resource.type == 'idle_screen_background' AND type == %@", self, strFormat];
  NSArray *files = [Files objectsWithPredicate:pred];
  if([files count]) {
    Files *f = [files objectAtIndex:0];
    background = [UIImage imageWithContentsOfFile:f.localUrl];
  }
  
  return background;
}

- (UIImage*)idleScreenLogo
{
  UIImage *logo = nil;
  
  NSString *strFormat = [Nodes stringForImageFormat:KKFormatOriginal];
  
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"resource.device == %@ AND resource.type == 'idle_screen_logo' AND type == %@", self, strFormat];
  NSArray *files = [Files objectsWithPredicate:pred];
  if([files count]) {
    Files *f = [files objectAtIndex:0];
    logo = [UIImage imageWithContentsOfFile:f.localUrl];
  }
  
  return logo;
}

@end
