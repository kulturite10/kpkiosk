// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Description.h instead.

#import <CoreData/CoreData.h>


extern const struct DescriptionAttributes {
	__unsafe_unretained NSString *desc;
	__unsafe_unretained NSString *language_code;
	__unsafe_unretained NSString *language_id;
	__unsafe_unretained NSString *language_title;
	__unsafe_unretained NSString *title;
} DescriptionAttributes;

extern const struct DescriptionRelationships {
	__unsafe_unretained NSString *resource;
} DescriptionRelationships;

extern const struct DescriptionFetchedProperties {
} DescriptionFetchedProperties;

@class Resources;







@interface DescriptionID : NSManagedObjectID {}
@end

@interface _Description : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (DescriptionID*)objectID;





@property (nonatomic, strong) NSString* desc;



//- (BOOL)validateDesc:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language_code;



//- (BOOL)validateLanguage_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* language_id;



@property int16_t language_idValue;
- (int16_t)language_idValue;
- (void)setLanguage_idValue:(int16_t)value_;

//- (BOOL)validateLanguage_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language_title;



//- (BOOL)validateLanguage_title:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Resources *resource;

//- (BOOL)validateResource:(id*)value_ error:(NSError**)error_;





@end

@interface _Description (CoreDataGeneratedAccessors)

@end

@interface _Description (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveDesc;
- (void)setPrimitiveDesc:(NSString*)value;




- (NSString*)primitiveLanguage_code;
- (void)setPrimitiveLanguage_code:(NSString*)value;




- (NSNumber*)primitiveLanguage_id;
- (void)setPrimitiveLanguage_id:(NSNumber*)value;

- (int16_t)primitiveLanguage_idValue;
- (void)setPrimitiveLanguage_idValue:(int16_t)value_;




- (NSString*)primitiveLanguage_title;
- (void)setPrimitiveLanguage_title:(NSString*)value;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;





- (Resources*)primitiveResource;
- (void)setPrimitiveResource:(Resources*)value;


@end
