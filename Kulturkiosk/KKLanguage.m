//
//  KKLanguage.m
//  Kulturkiosk
//
//  Created by Rune Botten on 16.10.12.
//
//

#import "KKLanguage.h"
#import "Nodes.h"

@implementation KKLanguage

+(NSArray*) availableLanguages
{
  NSMutableArray *languages = [NSMutableArray array];
  
  Nodes *rootNode = [Nodes rootNode];
  
  if([rootNode allChildrenHasContentForLanguage:KKLanguageTypeNorwegian])
    [languages addObject:@{@"title":@"Norsk",@"id":[NSNumber numberWithInt:KKLanguageTypeNorwegian], @"image" : [UIImage imageNamed:@"NO"]}];
  
  if([rootNode allChildrenHasContentForLanguage:KKLanguageTypeSwedish])
    [languages addObject:@{@"title":@"Svenska",@"id":[NSNumber numberWithInt:KKLanguageTypeSwedish],@"image":[UIImage imageNamed:@"SE"]}];
  
  if([rootNode allChildrenHasContentForLanguage:KKLanguageTypeEnglish])
    [languages addObject:@{@"title":@"English",@"id":[NSNumber numberWithInt:KKLanguageTypeEnglish],@"image":[UIImage imageNamed:@"GB"]}];
  
  if([rootNode allChildrenHasContentForLanguage:KKLanguageTypeFrench])
    [languages addObject:@{@"title":@"Français",@"id":[NSNumber numberWithInt:KKLanguageTypeFrench],@"image":[UIImage imageNamed:@"FR"]}];
  
  if([rootNode allChildrenHasContentForLanguage:KKLanguageTypeGerman])
    [languages addObject:@{@"title":@"Deutsch",@"id":[NSNumber numberWithInt:KKLanguageTypeGerman],@"image":[UIImage imageNamed:@"DE"]}];
  
  if([rootNode allChildrenHasContentForLanguage:KKLanguageTypeSpanish])
    [languages addObject:@{@"title":@"Español",@"id":[NSNumber numberWithInt:KKLanguageTypeSpanish],@"image":[UIImage imageNamed:@"ES"]}];
  return languages;
}

// Return the current language. Default for this key is set in the app delegate on launch.
+(KKLanguageType) currentLanguage
{
  NSNumber *lang = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentLanguage"];
  return (KKLanguageType)[lang intValue];
}

+(BOOL) setLanguage:(KKLanguageType) newLanguage
{
  if([KKLanguage currentLanguage] != newLanguage) { \
    [[NSUserDefaults standardUserDefaults] setInteger:newLanguage forKey:@"currentLanguage"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"languageChange" object:[NSNumber numberWithInt:newLanguage]];
    return YES;
  }
  return NO;
}

// Translate a string from the lang-%d.strings files where the int is the ID of the language (see KKLanguage above)
+(NSString*) localizedString:(NSString*) key
{
  KKLanguageType current = [KKLanguage currentLanguage];
  return [[NSBundle mainBundle] localizedStringForKey:key value:@"" table:[NSString stringWithFormat:@"lang-%d",current]];
}

@end
