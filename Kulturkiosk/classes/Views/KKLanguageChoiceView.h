//
//  KKLanguageChoiceView.h
//  Kulturkiosk
//
//  Created by Rune Botten on 23.08.12.
//
//

#import <UIKit/UIKit.h>

@interface KKLanguageChoiceView : UIView

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIImageView *flag, *checkMark;

@end
