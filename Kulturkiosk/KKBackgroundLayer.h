//
//  KKBackgroundLayer.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 6/12/13.
//
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface KKBackgroundLayer : NSObject

+ (CAGradientLayer*)blackGradient;

@end
