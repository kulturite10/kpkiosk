#import "Files.h"
#import <AVFoundation/AVFoundation.h>
#import "Resources.h"

@implementation Files

/*
 * Generate a thumbnail of the media
 * Pass a CGSize to have it scaled, or pass NULL if you dont care
 *
 * TODO: Expand to other media
 */
-(UIImage*) thumbnailWithSize:(CGSize*) size
{
  UIImage *theImage = nil;
  
  if([self.resource.type isEqualToString:KKResourceTypeVideo])
  {
    // Its a video. Take a screen grab from the middle point in the video.
    
    NSURL *url = [NSURL fileURLWithPath:self.localUrl];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    
    if(!asset.playable) {
      return nil;
    }
    
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.requestedTimeToleranceBefore = kCMTimeZero;
    generator.requestedTimeToleranceAfter  = kCMTimeZero;
    
    if(size != NULL)
      generator.maximumSize = *size;
    NSError *err = NULL;
    
    CMTime time = CMTimeMakeWithSeconds(CMTimeGetSeconds([asset duration])/2, 60);
    //NSLog(@"Generating thumbnail for %@", url);
    CGImageRef imgRef = [generator copyCGImageAtTime:time actualTime:NULL error:&err];
    if(imgRef == NULL) {
      NSLog(@"Error generating thumbnail: %@", [err localizedFailureReason]);
      return nil;
    }
    
    theImage = [[UIImage alloc] initWithCGImage:imgRef];
    CGImageRelease(imgRef);
  }
  return theImage;
}

@end
