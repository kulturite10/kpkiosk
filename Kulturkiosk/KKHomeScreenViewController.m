//
//  KKHomeScreenViewController.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 27.05.13.
//
//

#import "KKHomeScreenViewController.h"
#import "KKNavigationViewController.h"
#import "KKHomeScreenItem.h"
#import "KKPopoverBackgroundView.h"

@interface KKHomeScreenViewController ()

@property (nonatomic, strong) NSArray *presentations;
@property (weak, nonatomic) IBOutlet UIView *presentationsContainer;
@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) UIPopoverController *languageSelector;

@end

@implementation KKHomeScreenViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  Device *device=[Device findFirst];
  self.presentations = [device.presentations sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES]]];

 
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(languageChange)
                                               name:@"languageChange"
                                             object:nil];

  for (Presentation *presentation in self.presentations) {
    KKHomeScreenItem *homeScreenItem = [[[NSBundle mainBundle] loadNibNamed:@"HomeScreenItem" owner:self options:nil] objectAtIndex:0];
    for (Resources *resource in presentation.resources) {
      NSLog(@"%@",resource.type);
      if([resource.type isEqualToString: @"icon"]) {
        Files *file=[resource imageWithFormat:KKFormatOriginal];
        NSLog(@"resource %@ - %@",resource,file);
        homeScreenItem.iconView.image=[UIImage imageWithContentsOfFile: file.localUrl];
        break;
      }
    }
    homeScreenItem.iconView.image = [presentation icon];
    
    
    //   [homeScreenItem.languages setFont:[UIFont fontWithName:@"MyriadPro-Italic" size:18]];
    
    NSArray *content = [presentation.content sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES]]];
    for (Content *contentItem in content) {
      homeScreenItem.languages.text=[homeScreenItem.languages.text stringByAppendingFormat:@"%@\n",[contentItem title]];
    }
    
    [homeScreenItem.button setTag:presentation.identifierValue];
    [homeScreenItem.button addTarget:self action:@selector(didSelectPresentation:) forControlEvents:UIControlEventTouchUpInside];
    [homeScreenItem setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self.presentationsContainer addSubview:homeScreenItem];
  }
  
  self.background.image = device.idleScreenBackground;
  self.logo.image = device.idleScreenLogo;
	// Do any additional setup after loading the view.
}

- (void)languageChange
{
  if([self.languageSelector isPopoverVisible])
    [self.languageSelector dismissPopoverAnimated:YES];
}

- (void)viewDidLayoutSubviews
{
  [super viewDidLayoutSubviews];
  NSMutableDictionary *items = [NSMutableDictionary dictionary];
  int itemCount = 0;
  
  for (UIView *view in self.presentationsContainer.subviews) {
    [items setObject:view forKey:[NSString stringWithFormat:@"item%d", itemCount]];
    itemCount++;
  }
  
  NSMutableString *constraintString = [NSMutableString string];
  NSUInteger edgeSpacing = (self.presentationsContainer.frame.size.width - ((itemCount-1) * 10) - (160 * itemCount)) / 2;
  
  for (int i = 0; i < self.presentationsContainer.subviews.count; i++) {
    
    if (i == 0) {
      [constraintString appendFormat:@"|-%d-[%@(160)]", edgeSpacing, [items allKeysForObject:self.presentationsContainer.subviews[i]][0]];
    } else {
      [constraintString appendFormat:@"-10-[%@(160)]", [items allKeysForObject:self.presentationsContainer.subviews[i]][0]];
    }
    
  }
   
  if(self.presentationsContainer.subviews.count) {
    [self.presentationsContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:constraintString options:NSLayoutFormatAlignAllCenterY metrics:nil views:items]];
    [self.presentationsContainer setNeedsLayout];
  }
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue isKindOfClass:[UIStoryboardPopoverSegue class]])
  {
    self.languageSelector = [(UIStoryboardPopoverSegue*)segue popoverController];
    self.languageSelector.popoverBackgroundViewClass = [KKPopoverBackgroundView class];
    
    CGFloat popoverHeight = ([[KKLanguage availableLanguages] count] > 3 ? 200 : 100);
    self.languageSelector.popoverContentSize = CGSizeMake(630, popoverHeight);

  }
}

- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didSelectPresentation:(id)sender
{
  UIButton *button = (UIButton*) sender;
  KKNavigationViewController *navController = (KKNavigationViewController*) self.presentingViewController;
  [navController setShouldEnterRestmode:NO];
  UIButton *targetbutton = (UIButton*) [[navController buttonContainer] viewWithTag:button.tag];
  [targetbutton sendActionsForControlEvents:UIControlEventTouchUpInside];
  [self dismissViewControllerAnimated:YES completion:^{
      [navController setShouldEnterRestmode:YES];
  }];

}

@end
