//
//  KKHeaderCollectionView.h
//  Kulturkiosk
//
//  Created by Martin Jensen on 16.07.12.
//
//

#import <UIKit/UIKit.h>
#import "Nodes.h"

@interface KKHeaderCollectionView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *title, *text, *readMore;
@property (weak, nonatomic) IBOutlet UIImageView *featureImage;

@property (strong, nonatomic) Nodes *node;

@end
