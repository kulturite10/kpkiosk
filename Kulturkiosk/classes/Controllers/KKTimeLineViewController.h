//
//  KKTimeLineViewController2.h
//  Kulturkiosk
//
//  Created by Rune Botten on 06.09.12.
//
//

#import <UIKit/UIKit.h>
#import "KKNavBarController.h"

@interface KKTimeLineViewController : UICollectionViewController <UIScrollViewDelegate,KKNavBarControllerDelegate>
@property (strong, nonatomic) Presentation *presentation;
- (void)highlightNode:(Nodes*)node;
- (void)restoreScreen;
-(void) prepareContent;
@end
