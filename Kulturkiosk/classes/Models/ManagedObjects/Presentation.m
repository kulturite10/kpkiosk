#import "Presentation.h"

@implementation Presentation

-(NSArray*)files
{
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"resource.presentation == %@", self];
  
  return [Files objectsWithPredicate:pred];
}

- (Nodes*) node
{
  if (![[self.type lowercaseString] isEqualToString:@"single"] ) {
    return nil;
  }
  
  return [(Record*)[self.records anyObject] node];
}

- (UIImage *) icon
{
  UIImage *icon = [UIImage imageNamed:[NSString stringWithFormat:@"%@-idle.png", self.type]];
  
  NSString *strFormat = [Nodes stringForImageFormat:KKFormatOriginal];
  
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"resource.presentation == %@ AND resource.type == 'icon' AND type == %@", self, strFormat];
  NSArray *files = [Files objectsWithPredicate:pred];
  if([files count]) {
    Files *f = [files objectAtIndex:0];
    icon = [UIImage imageWithContentsOfFile:f.localUrl];
  }
  
  return icon;
}


- (UIImage*)timelineBackground
{
  UIImage *image = nil;
  
  NSString *strFormat = [Nodes stringForImageFormat:KKFormatOriginal];
  
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"resource.presentation == %@ AND resource.type == 'timeline_bg' AND type == %@", self, strFormat];
  NSArray *files = [Files objectsWithPredicate:pred];
  if([files count]) {
    Files *f = [files objectAtIndex:0];
    image = [UIImage imageWithContentsOfFile:f.localUrl];
  }
  
  return image;
}

- (UIImage*)timelineInterLayer
{
  UIImage *image = nil;
  
  NSString *strFormat = [Nodes stringForImageFormat:KKFormatOriginal];
  
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"resource.presentation == %@ AND resource.type == 'timeline_interlayer' AND type == %@", self, strFormat];
  NSArray *files = [Files objectsWithPredicate:pred];
  if([files count]) {
    Files *f = [files objectAtIndex:0];
    image = [UIImage imageWithContentsOfFile:f.localUrl];
  }
  
  return image;
  
}

- (UIImage*)mapBackground
{
  UIImage *map = nil;
  
  if (![self.type isEqualToString:@"map"])
    return map;
  
  NSString *strFormat = [Nodes stringForImageFormat:KKFormatOriginal];
  
  NSPredicate *pred = [NSPredicate predicateWithFormat:@"resource.presentation == %@ AND resource.type == 'map_bg' AND type == %@", self, strFormat];
  NSArray *files = [Files objectsWithPredicate:pred];
  if([files count]) {
    Files *f = [files objectAtIndex:0];
    map = [UIImage imageWithContentsOfFile:f.localUrl];
  }
  
  return map;
}

- (NSString*)className
{
  return [[self.type capitalizedString] stringByReplacingOccurrencesOfString: @"_" withString: @""];
}

-(Content *)contentForLanguage:(KKLanguageType)lang
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"presentation == %@ AND language_id == %d", self, lang];
  NSArray *content = [Content objectsWithPredicate:predicate];
  if ([content count] == 0) {
    return nil;
  }
  return [content objectAtIndex:0];
}


@end
