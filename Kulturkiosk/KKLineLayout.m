//
//  KKLineLayout.m
//  Kulturkiosk
//
//  Created by Rune Botten on 21.08.12.
//
//

#import "KKLineLayout.h"

@implementation KKLineLayout

#define ACTIVE_DISTANCE 200
#define ZOOM_FACTOR 0.5

-(id) init
{
  self = [super init];
  if(self)
  {
    self.itemSize = CGSizeMake(206, 165);
    self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.sectionInset = UIEdgeInsetsMake(50, 10, 10, 10);
//    self.minimumLineSpacing = 50.0;
  }
  return self;
}

-(BOOL) shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
  return YES;
}

-(NSArray*) layoutAttributesForElementsInRect:(CGRect)rect
{
  NSArray *array = [super layoutAttributesForElementsInRect:rect];
  
  CGRect visibleRect;
  visibleRect.origin = self.collectionView.contentOffset;
  visibleRect.size   = self.collectionView.bounds.size;
  
  for(UICollectionViewLayoutAttributes *attributes in array) {
    if(CGRectIntersectsRect(attributes.frame, rect)) {
      CGFloat distance = CGRectGetMidX(visibleRect) - attributes.center.x;
      CGFloat normalizedDistance = distance / ACTIVE_DISTANCE;
      if(ABS(distance) < ACTIVE_DISTANCE) {
        double zoom = 1 + ZOOM_FACTOR * (1 - ABS(normalizedDistance));
        attributes.transform3D = CATransform3DMakeScale(zoom, zoom, 1.0);
        attributes.zIndex = round(zoom);
      }
    }
  }
  
  return array;
}

-(CGPoint) targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset
                                 withScrollingVelocity:(CGPoint)velocity
{
  CGFloat offsetAdjustment = MAXFLOAT;
  CGFloat horizontalCenter = proposedContentOffset.x + (CGRectGetWidth(self.collectionView.bounds)) / (CGFloat)2.0;
  
  CGRect targetRect = CGRectMake(proposedContentOffset.x, 0.0, self.collectionView.bounds.size.width, self.collectionView.bounds.size.height);
  
  NSArray *array = [super layoutAttributesForElementsInRect:targetRect];
  
  for(UICollectionViewLayoutAttributes *layoutAttributes in array)
  {
    CGFloat itemHorizontalCenter = layoutAttributes.center.x;
    if (ABS(itemHorizontalCenter - horizontalCenter) < ABS(offsetAdjustment))
    {
      offsetAdjustment = itemHorizontalCenter - horizontalCenter;
    }
  }
  return CGPointMake(proposedContentOffset.x + offsetAdjustment, proposedContentOffset.y);
}

@end
