// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Device.m instead.

#import "_Device.h"

const struct DeviceAttributes DeviceAttributes = {
	.defaultPresentation = @"defaultPresentation",
	.identifier = @"identifier",
	.idleScreenActive = @"idleScreenActive",
	.idleScreenTitle = @"idleScreenTitle",
	.name = @"name",
	.resetTime = @"resetTime",
};

const struct DeviceRelationships DeviceRelationships = {
	.presentations = @"presentations",
	.resources = @"resources",
};

const struct DeviceFetchedProperties DeviceFetchedProperties = {
};

@implementation DeviceID
@end

@implementation _Device

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Device" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Device";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Device" inManagedObjectContext:moc_];
}

- (DeviceID*)objectID {
	return (DeviceID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"defaultPresentationValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"defaultPresentation"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"idleScreenActiveValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"idleScreenActive"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"resetTimeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"resetTime"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic defaultPresentation;



- (int16_t)defaultPresentationValue {
	NSNumber *result = [self defaultPresentation];
	return [result shortValue];
}

- (void)setDefaultPresentationValue:(int16_t)value_ {
	[self setDefaultPresentation:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveDefaultPresentationValue {
	NSNumber *result = [self primitiveDefaultPresentation];
	return [result shortValue];
}

- (void)setPrimitiveDefaultPresentationValue:(int16_t)value_ {
	[self setPrimitiveDefaultPresentation:[NSNumber numberWithShort:value_]];
}





@dynamic identifier;






@dynamic idleScreenActive;



- (BOOL)idleScreenActiveValue {
	NSNumber *result = [self idleScreenActive];
	return [result boolValue];
}

- (void)setIdleScreenActiveValue:(BOOL)value_ {
	[self setIdleScreenActive:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIdleScreenActiveValue {
	NSNumber *result = [self primitiveIdleScreenActive];
	return [result boolValue];
}

- (void)setPrimitiveIdleScreenActiveValue:(BOOL)value_ {
	[self setPrimitiveIdleScreenActive:[NSNumber numberWithBool:value_]];
}





@dynamic idleScreenTitle;






@dynamic name;






@dynamic resetTime;



- (int16_t)resetTimeValue {
	NSNumber *result = [self resetTime];
	return [result shortValue];
}

- (void)setResetTimeValue:(int16_t)value_ {
	[self setResetTime:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveResetTimeValue {
	NSNumber *result = [self primitiveResetTime];
	return [result shortValue];
}

- (void)setPrimitiveResetTimeValue:(int16_t)value_ {
	[self setPrimitiveResetTime:[NSNumber numberWithShort:value_]];
}





@dynamic presentations;

	
- (NSMutableSet*)presentationsSet {
	[self willAccessValueForKey:@"presentations"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"presentations"];
  
	[self didAccessValueForKey:@"presentations"];
	return result;
}
	

@dynamic resources;

	
- (NSMutableSet*)resourcesSet {
	[self willAccessValueForKey:@"resources"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"resources"];
  
	[self didAccessValueForKey:@"resources"];
	return result;
}
	






@end
