#import "Content.h"
#import <AVFoundation/AVAsset.h>
#import "Nodes.h"
#import "Resources.h"

@implementation Content

/*
 * This method returns all resources matching the type and language.
 *
 * type : 'image', 'video', 'document' etc...
 *
 */
-(NSArray*) resourcesWithType:(KKResourceType) type
{
  if(type == KKResourceTypeImage) {
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"node == %@ AND type == 'image'", self.node];
    
    if ([self.node nodeType] == KKTypeRecord) {
      pred = [NSPredicate predicateWithFormat:@"node.parent_id == %@ AND type == 'image'", self.node];
    }
    
    NSArray *array = [Resources objectsWithPredicate:pred];
    return [array sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES]]];
  }
  
  NSFetchRequest *fetch = [Description fetchRequest];
  
  NSPredicate *predicate;
  
  if([self.node nodeType] == KKTypeRecord) {
    predicate = [NSPredicate predicateWithFormat:@"resource.node.parent_id == %@ AND resource.type == %@", self.node, type];
  } else {
    predicate = [NSPredicate predicateWithFormat:@"resource.node == %@ AND resource.type == %@", self.node, type];
  }
  
  [fetch setPredicate:predicate];
  
  NSArray *descs = [Description objectsWithFetchRequest:fetch];
  
  // Collect the resources
  NSMutableArray *res = [[NSMutableArray alloc] initWithCapacity:[descs count]];
  for(Description *d in descs) {
    [res addObject:d.resource];
  }
  
  if(type == KKResourceTypeVideo) {
    // Filter unwatchable videos
    NSMutableArray *videos = [NSMutableArray array];
    for(Resources *r in res) {
      Files *video = [r.files allObjects][0];
      NSURL *url = [NSURL fileURLWithPath:video.localUrl];
      AVURLAsset *asset = [AVURLAsset assetWithURL:url];
      if(asset.playable)
        [videos addObject:r];
    }
    res = videos;
  }
  
  return res;
}

@end
