//
//  KKWindow.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 10.12.12.
//
//

#import "KKWindow.h"

@implementation KKWindow

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


- (void)sendEvent:(UIEvent *)event
{
  [super sendEvent:event];
  UITouch *touch = [[event allTouches] anyObject];
  if (event.type == UIEventTypeTouches && touch.phase == UITouchPhaseEnded && UIAppDelegate.shouldRegisterTouch){
    
    [[NSNotificationCenter defaultCenter] postNotificationName:UserDidTouchWindowNotification object:nil userInfo:nil];
  }
  return;
}


@end
