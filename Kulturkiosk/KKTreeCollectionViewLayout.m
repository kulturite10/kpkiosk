//
//  KKTreeCollectionViewLayout.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 22.03.13.
//
//

#import "KKTreeCollectionViewLayout.h"

@implementation KKTreeCollectionViewLayout

- (id)initWithCoder:(NSCoder *)aDecoder
{
  self = [super initWithCoder:aDecoder];
  
  if (self) {
    self.itemSize = CGSizeMake(300, 244);
    self.sectionInset = UIEdgeInsetsMake(55, 10, 55, 10);
    self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.minimumInteritemSpacing = 10;
  }
  return self;
}

- (CGSize)collectionViewContentSize
{
  CGFloat width = [self.collectionView numberOfItemsInSection:0] * self.itemSize.width + (([self.collectionView numberOfItemsInSection:0] -1) * self.minimumInteritemSpacing) + self.sectionInset.left + self.sectionInset.right;
  return CGSizeMake(width, self.itemSize.height);
}

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
  return YES;
}

@end
