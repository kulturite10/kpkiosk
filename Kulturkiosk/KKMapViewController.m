//
//  KKMapViewController.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 31.05.13.
//
//

#import "KKMapViewController.h"
#import "KKCalloutViewController.h"
#import "KKMapView.h"
#import "KKAnnotationView.h"
#import "KKAnnotation.h"
#import "KKCalloutView.h"
#import "UIColor+HexColor.h"

@interface UIImage (Resize)
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
@end
@implementation UIImage (Resize)

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    //UIGraphicsBeginImageContext(newSize);
  UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
  [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
  UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return newImage;
}
@end


@interface KKMapViewController () <KKMapViewDelegate, UIPopoverControllerDelegate>
@property (weak, nonatomic) IBOutlet KKMapView *mapView;
@property (weak, nonatomic) KKAnnotation *selectedAnnotation;
@property (strong, nonatomic) KKCalloutViewController *calloutView;
@property (nonatomic,assign) BOOL active;


- (void) annotateMap;
-(void) languageChange;

@end

@implementation KKMapViewController

- (void)dealloc
{
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  self.navigationController.navigationBarHidden = YES;
  
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTapCallout:) name:@"didTapCallout" object:nil];
  
  UIImage *mapBackground = [self.presentation mapBackground];
  
  if (mapBackground.size.width < 1024) {
    CGFloat scaleFactor = 1046 / mapBackground.size.height;
    CGSize imageSize = CGSizeMake(floorf(mapBackground.size.width*scaleFactor), floorf(mapBackground.size.height*scaleFactor));
    mapBackground = [UIImage imageWithImage:[self.presentation mapBackground] scaledToSize:imageSize];
  }
  
  [self.mapView displayMap:mapBackground];
  self.mapView.backgroundColor = [UIColor lightGrayColor];
  self.mapView.mapViewdelegate = self;
  
  if(self.presentation) {
    [self annotateMap];
    self.calloutView = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil]  instantiateViewControllerWithIdentifier:@"CalloutView"];
  }

  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(languageChange)
                                               name:@"languageChange"
                                             object:nil];

}

-(void) languageChange
{
  [self.mapView removeAnnotations];
  [self annotateMap];
}

- (void)viewWillDisappear:(BOOL)animated
{
  [self didDeselectAnnotation: self.selectedAnnotation];
  self.active=NO;
}

- (void)viewWillAppear:(BOOL)animated{
  self.active=YES;
}


- (void) annotateMap
{
  for (Record *record in [[self presentation] records]) {
    [self addAnnotationForRecord: record];
  }

}
  
- (void)addAnnotationForRecord:(Record*)record
{
  
  int x = [record.x intValue];
  int y = [record.y intValue];
  
  CGPoint point = CGPointMake(x*self.mapView.annotationOffset, y*self.mapView.annotationOffset);
  
  KKAnnotation *annotation = [KKAnnotation annotationWithPoint:point];

  Content *content= [record.node contentForLanguage:[KKLanguage currentLanguage]];
  annotation.tag              = record.node.item_id;
  annotation.title            = content.title;
  annotation.subtitle         = content.subtitle;
  annotation.ingress          = content.desc;
  annotation.thumbnail        = record.node.mainImage;
  annotation.code             = record.code;
  annotation.backgroundColor  = [UIColor colorWithHexColor:(record.color ? : @"#f59711")];

  [self.mapView addAnnotation:annotation animated:NO];

}

- (void)highlightNode:(Nodes *)node
{
  
  for (KKAnnotationView *annotationView in self.mapView.annotationViews) {
    KKAnnotation *annotation = annotationView.annotation;
    if ([annotation.tag intValue] == [node.item_id intValue]) {
      [self.mapView showCallOut: [annotationView.gestureRecognizers objectAtIndex:0]];
    }
  }
}

- (void)didTapCallout:(id) sender
{
  [self.selectedAnnotation setSelected:NO];
  [self.presentingPopover dismissPopoverAnimated:NO];
  }

- (void)willSelectAnnotation:(KKAnnotation *)annotation
{
  if (self.selectedAnnotation != annotation)
  {
    [self.selectedAnnotation setSelected:NO];
    [self didDeselectAnnotation:self.selectedAnnotation];
  }
}

- (void)didSelectAnnotation:(KKAnnotation *)annotation
{
  self.selectedAnnotation = annotation;

  // Don't present annotations when we're not on screen.
  if(!self.active) return;
  
  UIPopoverController *popC = [[UIPopoverController alloc] initWithContentViewController:self.calloutView];
  popC.popoverBackgroundViewClass = [KKCalloutView class];
  popC.delegate = self;
  popC.passthroughViews = (NSArray*) self.mapView.annotationViews;
  
  if([self.presentingPopover isPopoverVisible]) {
    [self.presentingPopover dismissPopoverAnimated:NO];
  }
  self.presentingPopover = popC;
  
  CGFloat x = (CGFloat) round((self.mapView.zoomScale * annotation.point.x) - (45 / 2));
  CGFloat y = (CGFloat) round((self.mapView.zoomScale * annotation.point.y) - (70 / 2));

  CGRect frame = CGRectMake(x,y, 1, 1);
  
  [self.presentingPopover presentPopoverFromRect:frame inView:self.mapView permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
  [self.calloutView setAnnotation:annotation];
}


- (void)didDeselectAnnotation:(KKAnnotation *)annotation
{
  if([self.presentingPopover isPopoverVisible]) {
    [self.presentingPopover dismissPopoverAnimated:NO];
  }
  [self.selectedAnnotation setSelected:NO];
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
  [self.selectedAnnotation setSelected:NO];
  
  return YES;
}

-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
 
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  
}

@end
