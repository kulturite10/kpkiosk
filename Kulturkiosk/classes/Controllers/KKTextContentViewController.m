//
//  KKTextContentViewController.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 29.05.12.
//  Copyright (c) 2012 Nordaaker Ltd. All rights reserved.
//

#import "KKTextContentViewController.h"
#import "GRMustache.h"
#import "Resources.h"
#import "Description.h"
#import "Credit.h"

@interface KKImageRenderObject : NSObject
@property (nonatomic, strong) NSString *urlString;
@property (nonatomic, strong) NSArray *credits, *descs;
@end
@implementation KKImageRenderObject
@end
@interface KKRenderObject : NSObject
@property (nonatomic, strong) NSString *title, *ingress, *body;
@property (nonatomic, strong) NSArray *images;
@end

@implementation KKRenderObject
@end

@interface KKTextContentViewController () {
  int numPages, numPagesLoaded;
}

- (IBAction)goBack:(id)sender;
- (IBAction)changePage:(id)sender;

@end

@implementation KKTextContentViewController

- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  self.scrollView.delegate = self;
  
  NSString *template = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"article" ofType:@"html"] encoding:NSUTF8StringEncoding error:nil];
	
  NSArray *pages = [self.node pagesForLanguage:[KKLanguage currentLanguage]];
  
  if([pages count] == 0) {
    [self.spinner stopAnimating];
    return;
  }
  CGRect webViewFrame = CGRectMake(0, 0, 1024, 768);
  
  self.pageControl.numberOfPages = numPages = numPagesLoaded = 0;
  for( Content *page in pages) {
    KKRenderObject *renderObj = [[KKRenderObject alloc] init];
    renderObj.title = page.title;
    renderObj.ingress = page.ingress;
    renderObj.body = page.body;
    
    NSArray *images = [page resourcesWithType:KKResourceTypeImage];
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:[images count] ];
    for(Resources *image in images) {
      Files *file = [image imageWithFormat:KKFormatSmall];
      if(file) {
        KKImageRenderObject *imageRenderObject = [[KKImageRenderObject alloc] init];
        
        NSMutableArray *credits = [NSMutableArray arrayWithCapacity:[image.credits count]];
        
        for(Credit *c in [image.credits allObjects]) {
          [credits addObject:[c localizedCredit]];
        }
        
        imageRenderObject.urlString = file.localUrl;
        imageRenderObject.descs = [image descriptionsForLanguage:[KKLanguage currentLanguage]];
        imageRenderObject.credits = credits;
        
        [array addObject:imageRenderObject];
      }
    }
    renderObj.images = array;
    
    NSError *error = nil;
    
    NSString *rendering = [GRMustacheTemplate renderObject:renderObj
                                                fromString:template
                                                     error:&error];
   
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:webViewFrame];
    
    webView.delegate = self;
    webView.backgroundColor = [UIColor clearColor];
    webView.opaque = NO;
    
    [webView loadHTMLString:rendering baseURL:[NSURL fileURLWithPath:@"/"]];
    
    [self.scrollView addSubview:webView];
    webViewFrame = CGRectOffset(webViewFrame, webViewFrame.size.width, 0);
    
    CGSize contentSize = self.scrollView.contentSize;
    contentSize.width += webViewFrame.size.width;
    self.scrollView.contentSize = contentSize;
    numPages++;
  }
  self.pageControl.currentPage = 0;
}

- (IBAction)goBack:(id)sender {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)changePage:(id)sender {
  
  int x = self.pageControl.currentPage * (int)self.scrollView.frame.size.width;
  [self.scrollView setContentOffset:CGPointMake(x, 0) animated:YES];
  
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
  self.pageControl.numberOfPages += 1;
  
  //webView.backgroundColor = [UIColor blackColor];
  webView.opaque = YES;
  
  if(++numPagesLoaded == 1) {
    [self.spinner stopAnimating];
    [self.scrollView setHidden:NO];
  }
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView
{
  if(scrollView == self.scrollView)
    self.pageControl.currentPage = [self currentPage];
}

//-(void) scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
//{
//  if(scrollView != self.scrollView)
//    return;
//  
//  if(scale == 1.0) {
//    for(UIView *v in self.scrollView.subviews)
//      v.hidden = NO;
//    self.scrollView.pagingEnabled = YES;
//  }
//}

-(NSUInteger) currentPage
{
  return (NSUInteger) round(self.scrollView.contentOffset.x / 1024);
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
  return [self.scrollView.subviews objectAtIndex:[self currentPage]];
}

-(void) dealloc
{
  self.node = nil;
}

@end
