//
//  KKTopAlignImageView.h
//  Kulturkiosk
//
//  Created by Rune Botten on 21.08.12.
//
// http://stackoverflow.com/questions/3272335/alignment-uiimageview-with-aspect-fit
//

#import <UIKit/UIKit.h>

@interface KKTopAlignImageView : UIView {
  UIImageView *_imageView;
}

@property (nonatomic, strong) UIImage *image;

@end
