// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Credit.h instead.

#import <CoreData/CoreData.h>


extern const struct CreditAttributes {
	__unsafe_unretained NSString *credit_type;
	__unsafe_unretained NSString *name;
} CreditAttributes;

extern const struct CreditRelationships {
	__unsafe_unretained NSString *resource;
} CreditRelationships;

extern const struct CreditFetchedProperties {
} CreditFetchedProperties;

@class Resources;




@interface CreditID : NSManagedObjectID {}
@end

@interface _Credit : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (CreditID*)objectID;





@property (nonatomic, strong) NSString* credit_type;



//- (BOOL)validateCredit_type:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Resources *resource;

//- (BOOL)validateResource:(id*)value_ error:(NSError**)error_;





@end

@interface _Credit (CoreDataGeneratedAccessors)

@end

@interface _Credit (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveCredit_type;
- (void)setPrimitiveCredit_type:(NSString*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;





- (Resources*)primitiveResource;
- (void)setPrimitiveResource:(Resources*)value;


@end
