// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Content.h instead.

#import <CoreData/CoreData.h>


extern const struct ContentAttributes {
	__unsafe_unretained NSString *body;
	__unsafe_unretained NSString *created_at;
	__unsafe_unretained NSString *desc;
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *ingress;
	__unsafe_unretained NSString *language_code;
	__unsafe_unretained NSString *language_id;
	__unsafe_unretained NSString *language_title;
	__unsafe_unretained NSString *position;
	__unsafe_unretained NSString *subtitle;
	__unsafe_unretained NSString *title;
	__unsafe_unretained NSString *updated_at;
} ContentAttributes;

extern const struct ContentRelationships {
	__unsafe_unretained NSString *links;
	__unsafe_unretained NSString *node;
	__unsafe_unretained NSString *presentation;
} ContentRelationships;

extern const struct ContentFetchedProperties {
} ContentFetchedProperties;

@class Links;
@class Nodes;
@class Presentation;














@interface ContentID : NSManagedObjectID {}
@end

@interface _Content : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ContentID*)objectID;





@property (nonatomic, strong) NSString* body;



//- (BOOL)validateBody:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* created_at;



//- (BOOL)validateCreated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* desc;



//- (BOOL)validateDesc:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* identifier;



@property int16_t identifierValue;
- (int16_t)identifierValue;
- (void)setIdentifierValue:(int16_t)value_;

//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* ingress;



//- (BOOL)validateIngress:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language_code;



//- (BOOL)validateLanguage_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* language_id;



@property int16_t language_idValue;
- (int16_t)language_idValue;
- (void)setLanguage_idValue:(int16_t)value_;

//- (BOOL)validateLanguage_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language_title;



//- (BOOL)validateLanguage_title:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* position;



@property int16_t positionValue;
- (int16_t)positionValue;
- (void)setPositionValue:(int16_t)value_;

//- (BOOL)validatePosition:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* subtitle;



//- (BOOL)validateSubtitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* updated_at;



//- (BOOL)validateUpdated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *links;

- (NSMutableSet*)linksSet;




@property (nonatomic, strong) Nodes *node;

//- (BOOL)validateNode:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) Presentation *presentation;

//- (BOOL)validatePresentation:(id*)value_ error:(NSError**)error_;





@end

@interface _Content (CoreDataGeneratedAccessors)

- (void)addLinks:(NSSet*)value_;
- (void)removeLinks:(NSSet*)value_;
- (void)addLinksObject:(Links*)value_;
- (void)removeLinksObject:(Links*)value_;

@end

@interface _Content (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveBody;
- (void)setPrimitiveBody:(NSString*)value;




- (NSString*)primitiveCreated_at;
- (void)setPrimitiveCreated_at:(NSString*)value;




- (NSString*)primitiveDesc;
- (void)setPrimitiveDesc:(NSString*)value;




- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int16_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int16_t)value_;




- (NSString*)primitiveIngress;
- (void)setPrimitiveIngress:(NSString*)value;




- (NSString*)primitiveLanguage_code;
- (void)setPrimitiveLanguage_code:(NSString*)value;




- (NSNumber*)primitiveLanguage_id;
- (void)setPrimitiveLanguage_id:(NSNumber*)value;

- (int16_t)primitiveLanguage_idValue;
- (void)setPrimitiveLanguage_idValue:(int16_t)value_;




- (NSString*)primitiveLanguage_title;
- (void)setPrimitiveLanguage_title:(NSString*)value;




- (NSNumber*)primitivePosition;
- (void)setPrimitivePosition:(NSNumber*)value;

- (int16_t)primitivePositionValue;
- (void)setPrimitivePositionValue:(int16_t)value_;




- (NSString*)primitiveSubtitle;
- (void)setPrimitiveSubtitle:(NSString*)value;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;




- (NSString*)primitiveUpdated_at;
- (void)setPrimitiveUpdated_at:(NSString*)value;





- (NSMutableSet*)primitiveLinks;
- (void)setPrimitiveLinks:(NSMutableSet*)value;



- (Nodes*)primitiveNode;
- (void)setPrimitiveNode:(Nodes*)value;



- (Presentation*)primitivePresentation;
- (void)setPrimitivePresentation:(Presentation*)value;


@end
