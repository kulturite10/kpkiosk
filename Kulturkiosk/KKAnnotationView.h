//
//  KKAnnotation.h
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 04.06.13.
//
//

#import <UIKit/UIKit.h>
#import "KKAnnotation.h"
#import "KKMapView.h"

@interface KKAnnotationView : UIView

@property (nonatomic, strong) KKAnnotation *annotation;
@property (nonatomic, strong) UIColor *bgColor;
@property (nonatomic, copy) NSString *code;
@property (nonatomic, assign) BOOL          animating;

- (id)initWithAnnotation:(KKAnnotation *)annotation onMapView:(KKMapView *)mapView;



@end
