//
//  KKContentViewController2.h
//  Kulturkiosk
//
//  Created by Rune Botten on 02.10.12.
//
//

#import "KKFolderViewController.h"

@interface KKContentViewController : KKFolderViewController

@property (strong) Presentation *presentation;

@end
