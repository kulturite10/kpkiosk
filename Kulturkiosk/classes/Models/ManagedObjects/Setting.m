#import "Setting.h"

@implementation Setting

+ (NSString *) valueForKey:(NSString*) key
{
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identifier == %@", key];
  NSArray *settings = [Setting objectsWithPredicate:predicate];
  if ([settings count] == 0) {
    return nil;
  }
  Setting *s = [settings objectAtIndex:0];
  
  return s.value;
}

@end
