//
//  KKCustomOverlaySegue.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 8/9/13.
//
//

#import "KKCustomOverlaySegue.h"

@implementation KKCustomOverlaySegue

- (void)perform
{
  UIViewController *src = (UIViewController *) self.sourceViewController;
  UIViewController *dst = (UIViewController *) self.destinationViewController;
  
  [dst.view setFrame:CGRectMake(0, 768, 1024, 768)];
  
  [src.view addSubview:dst.view];
  [src addChildViewController:dst];
  
  [UIView transitionWithView:src.view duration:0.3
                     options:UIViewAnimationOptionTransitionNone
                  animations:^{
                    [dst.view setFrame:CGRectMake(0,0, 1024, 768)];
                  }
                  completion:^(BOOL finished) {
                    [dst didMoveToParentViewController:src];
                  }];
}

@end
