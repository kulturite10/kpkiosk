//
//  KKZoomingImageView.h
//  Kulturkiosk
//
//  Created by Rune Botten on 11.12.12.
//
//

#import <UIKit/UIKit.h>

@interface KKZoomingImageView : UIScrollView
@property (strong, nonatomic) UIImage *image;
@property (assign) BOOL zoomEnabled;
@end
