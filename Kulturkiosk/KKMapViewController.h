//
//  KKMapViewController.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 31.05.13.
//
//

#import <UIKit/UIKit.h>

#import "KKPresentationViewController.h"

@interface KKMapViewController : KKPresentationViewController
@property (strong, nonatomic) UIPopoverController *presentingPopover;
@end
