//
//  KKRecordCollectionViewController.h
//  Kulturkiosk
//
//  Created by Rune Botten on 13.08.12.
//
//

#import <UIKit/UIKit.h>
#import "KKTreeCollectionViewController.h"
#import "Nodes.h"

@interface KKTreeContentRowController : KKTreeCollectionViewController

@property (strong) Nodes *node;

@end
