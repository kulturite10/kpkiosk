//
//  KKObjectCache.m
//  Kulturkiosk
//
//  Created by Martin Jensen on 13.07.12.
//  Copyright (c) 2012 Nordaaker Ltd.. All rights reserved.
//

#import "KKNodeObjectCache.h"

@implementation KKNodeObjectCache

-(NSManagedObject *)findInstanceOfEntity:(NSEntityDescription *)entity withPrimaryKeyAttribute:(NSString *)primaryKeyAttribute value:(id)primaryKeyValue inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    [request setEntity: entity];
    [request setFetchLimit: 1];
    [request setPredicate:[NSPredicate predicateWithFormat:@"%K = %@", primaryKeyAttribute, primaryKeyValue]];
    
    NSArray *results = [NSManagedObject executeFetchRequest:request inContext: managedObjectContext];
    if ([results count] == 0)
    {
        return nil;
    }
    return [results objectAtIndex:0];  
}

-(void)didDeleteObject:(NSManagedObject *)object
{
    NSLog(@"Deleted object: %@", object);
}

-(void)didCreateObject:(NSManagedObject *)object
{
    NSLog(@"Did add object: %@", object);
}

@end
