//
//  KKAppDelegate.m
//  Kulturkiosk
//
//  Created by Anders Wiik on 21.05.12.
//  Copyright (c) 2012 Nordaaker Ltd.. All rights reserved.
//

#import "KKAppDelegate.h"
#import "KKNodeObjectCache.h"
#import "KKWarningView.h"
#import "KKWindow.h"
#import "KKLanguage.h"
#import "KKLoadingViewController.h"
#import "KKNavigationViewController.h"
#import "KKMapViewController.h"
#import "KKVideoPresentationViewController.h"
#import "KKHomeScreenViewController.h"

typedef void( ^TimerCallback)();

@interface KKAppDelegate() <RKManagedObjectStoreDelegate>{
  __block BOOL _warningDisplayed,_timerRunning;
}

@property (nonatomic, assign) int inactivityTimeout, remainingSeconds;
@property (nonatomic, strong) RKReachabilityObserver *reachabilityObserver;
@property (nonatomic, strong) UIAlertView *alertView;
@property (nonatomic, strong) UIAlertView *timerAlertView;
@property (strong, nonatomic) NSDate *fireDate;
@property (assign) BOOL newProject;

@end
@implementation KKAppDelegate

@synthesize window = _window;
@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

-(void) loadLocalData
{
  // Every time
  [self deleteCurrentCachedData];
  [self setupRestKit];
  
  NSString* path = [[NSBundle mainBundle] pathForResource:@"data"
                                                   ofType:@"json"];
  NSString* JSONString = [NSString stringWithContentsOfFile:path
                                                encoding:NSUTF8StringEncoding
                                                   error:NULL];
  
  NSString* MIMEType = @"application/json";
  NSError* error = nil;
  id<RKParser> parser = [[RKParserRegistry sharedRegistry] parserForMIMEType:MIMEType];
  id parsedData = [parser objectFromString:JSONString error:&error];
  if (parsedData == nil && error) {
    NSLog(@"Error parsing local JSON data");
    return;
  }
  
  RKObjectMappingProvider* mappingProvider = [RKObjectManager sharedManager].mappingProvider;
  RKObjectMapper* mapper = [RKObjectMapper mapperWithObject:parsedData mappingProvider:mappingProvider];
  RKObjectMappingResult* result = [mapper performMapping];
  if (result) {
    [[RKObjectManager sharedManager].objectStore.managedObjectContextForCurrentThread save:nil];
  } else {
    NSLog(@"Error mapping local JSON data");
    return;
  }
  [self objectLoaderDidFinishLoading:nil];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  // Register the preference defaults early.
  [defaults registerDefaults:@{
   @"device_id" : @2,
   @"currentLanguage" : [NSNumber numberWithInt:KKLanguageTypeNorwegian],
   @"api_host" : @"http://api.ekp.no/",
   @"inactivity_timer": @1
   }];
  [defaults synchronize];
  
  // transfer the current version number into the defaults so that this correct value will be displayed when the user visit settings page later
  NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
  [defaults setObject:version forKey:@"version"];
  [defaults synchronize];
  
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneLoading) name:@"loadingFinished" object:nil];
 
  // Load the configured project
  NSInteger project_id   = [defaults integerForKey:@"device_id"],
  last_project = [defaults integerForKey:@"last_project"];
  
  self.newProject = project_id != last_project;
  
  // Has the user switched project via system settings since last launch?
  if(self.newProject) {
    [self deleteCurrentCachedData];
  }
  
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(reachabilityDetermined:)
                                               name:RKReachabilityWasDeterminedNotification object:nil];
  
  NSURL *reachabilityURL = [NSURL URLWithString:[defaults valueForKey:@"api_host"]];
  self.reachabilityObserver = [RKReachabilityObserver reachabilityObserverForHost:reachabilityURL.host];

  
  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
  KKLoadingViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"LoadingView"];
  self.window = [[KKWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
  self.window.rootViewController = viewController;

  self.window.opaque = YES;
  self.window.backgroundColor = [UIColor blackColor];
  [self.window makeKeyAndVisible];
  return YES;
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  exit(0);
}

- (void)startTimer
{
  if (self.inactivityTimeout == 0)
    return;
 
  _timerRunning = YES;
  _warningDisplayed = NO;
  
  self.fireDate = [[NSDate date] dateByAddingTimeInterval: self.inactivityTimeout];
  [self checkCountdownStatus];
}

- (void)checkCountdownStatus
{
  NSTimeInterval left = [self.fireDate timeIntervalSinceDate:[NSDate date]];
  if (!_timerRunning) {
    return;
  }
    
  KKNavigationViewController *navigationViewController = (KKNavigationViewController*) self.window.rootViewController;
  UIViewController *vc = nil;
  
  if ([navigationViewController.presentedViewController isKindOfClass:[KKHomeScreenViewController class]]) {
    return;
  }
  
  if (navigationViewController.activeViewController.presentedViewController) {
    vc = navigationViewController.activeViewController.presentedViewController;
  }else {
    vc = self.window.rootViewController;
  }

  if (left < 10 && !_warningDisplayed) {

    if ([navigationViewController.activeViewController isKindOfClass: [KKMapViewController class]] && [[(KKMapViewController*)navigationViewController.activeViewController presentingPopover] isPopoverVisible]) {
      [[(KKMapViewController*)navigationViewController.activeViewController presentingPopover] dismissPopoverAnimated: NO];
    }
    KKWarningView *view = [[[NSBundle mainBundle] loadNibNamed:@"KKWarningView" owner:self options:0] objectAtIndex:0];
    
    view.warningTitle.text = [KKLanguage localizedString:@"TimeoutTitle"];
    view.warningMessage.text = [KKLanguage localizedString:@"TimeoutMessage"];
    
    UIView *presentingView = vc.view;
    
    if (vc.presentedViewController){
      presentingView = vc.presentedViewController.view;
    }
    
    [presentingView addSubview:view];
    _warningDisplayed = YES;
  }
  
  if (left < 0) {
    
    if ([[navigationViewController.activeViewController presentedViewController] isKindOfClass: [KKPresentationViewController class]]) {
      [(KKVideoPresentationViewController*)[navigationViewController.activeViewController presentedViewController] dismissController: self];
    }
    for (UIView *view in [vc.view subviews]) {
      if ([view isKindOfClass:[KKWarningView class]]) {
        [view removeFromSuperview];
      }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:TimerDidEndNotification object:nil];
    _timerRunning = NO;
    return;
  }
  
  __block long long delayInNanoSeconds = (long long)(1.0 * NSEC_PER_SEC);
  
  dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInNanoSeconds);
  dispatch_after(popTime, dispatch_get_main_queue(), ^{
    [self checkCountdownStatus];
  });
  
}

- (void)restartTimer
{
  [self stopTimer];
  [self startTimer];
}

- (void)stopTimer
{
  _timerRunning = NO;
  self.remainingSeconds = 0;
}

- (void)reachabilityDetermined:(NSNotification *)notification {
  NSNotificationCenter* center = [NSNotificationCenter defaultCenter];
  [center removeObserver:self name:RKReachabilityWasDeterminedNotification object:nil];
  
  RKReachabilityObserver* observer = (RKReachabilityObserver *) [notification object];
  RKReachabilityNetworkStatus status = [observer networkStatus];
  if (RKReachabilityNotReachable == status && self.newProject) {
    RKLogInfo(@"No network access!");
    self.alertView = [[UIAlertView alloc] initWithTitle:@"Missing network connection?"
                                                    message:@"Must be able to connect to backend to load new project."
                                                   delegate:nil
                                          cancelButtonTitle:@"Quit"
                                          otherButtonTitles:nil];
    self.alertView.delegate = self;
    [self.alertView show];
  } else {
    [self setupRestKit];
    [self loadProjectFromNetwork];
  }
}

-(void) setDefaultLanguage
{
  NSArray *availableLanguages = [KKLanguage availableLanguages];
  if ([availableLanguages count]) {
    NSDictionary *firstLanguage = [availableLanguages objectAtIndex:0];
    KKLanguageType defaultLanguage = (KKLanguageType)[[firstLanguage objectForKey:@"id"] intValue];
    [KKLanguage setLanguage:defaultLanguage];
    DLog(@"Language set to %@", [firstLanguage valueForKey:@"title"]);
  }

}

- (void)doneLoading
{

  //NSString *defaultPresentation = [Setting valueForKey:@"defaultPresentation"];
  //NSLog(@"defaultPresentation = %@", defaultPresentation);
  //NSLog(@"all settings: %@", [[Setting allObjects] description]);
  
  KKAssetsManager *assetsManager = [KKAssetsManager sharedManager];
  // Make sure no extra requests has snuck into the queue in the mean time
  // In that case, we just wait some more
  if([assetsManager.queue count] > 1)
    return;
  
  [self setDefaultLanguage];

  [[NSNotificationCenter defaultCenter] removeObserver:self name:@"loadingFinished" object:nil];
  [[NSNotificationCenter defaultCenter] removeObserver:self.window.rootViewController name:@"loadedFile" object:nil];
  
  // Save the database, so all the downloaded data is retained
  [[RKObjectManager sharedManager].objectStore.managedObjectContextForCurrentThread save:nil];

  // Remember what project the database contains, so we can delete if it changes
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  [defaults setInteger:[defaults integerForKey:@"device_id"] forKey:@"last_project"];
  [defaults synchronize];
  
  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
  UIViewController *view = [storyboard instantiateInitialViewController];
  self.window.rootViewController = view;
  
 Device *device=[Device objectWithPredicate: [NSPredicate predicateWithFormat: @"identifier == %@" argumentArray:@[[[NSUserDefaults standardUserDefaults] objectForKey: @"device_id"]]]];

  if (device.idleScreenActiveValue) {
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier: @"HomeScreen"];
    [view presentViewController:viewController  animated:YES completion:nil];
  }
  self.inactivityTimeout = device.resetTimeValue;

  self.shouldRegisterTouch = YES;
  
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(restartTimer) name:UserDidTouchWindowNotification object:nil];

}

-(void) deleteCurrentCachedData
{
  // Delete current database to make room for different data
  NSLog(@"Deleting previous database");
  NSPersistentStoreCoordinator *storeCoordinator = [self persistentStoreCoordinator];
  NSArray *stores = [storeCoordinator persistentStores];
  for(NSPersistentStore *store in stores) {
    [storeCoordinator removePersistentStore:store error:nil];
    [[NSFileManager defaultManager] removeItemAtPath:store.URL.path error:nil];
  }
  
  // Delete the cache dir
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
  NSString *dir = [paths objectAtIndex:0]; // Get caches folder
  NSString *cacheDir = [dir stringByAppendingPathComponent:@"DownloadedMedia"];
  if ([[NSFileManager defaultManager] fileExistsAtPath:cacheDir])
  {
    NSLog(@"Deleting %@", cacheDir);
    [[NSFileManager defaultManager] removeItemAtPath:cacheDir error:nil];
  }

}

-(void)setupRestKit
{
  // Initialize RestKit
  RKObjectManager* objectManager = [RKObjectManager objectManagerWithBaseURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] valueForKey:@"api_host"]]];
  [RKObjectManager setSharedManager:objectManager];

  // Enable automatic network activity indicator management
  objectManager.client.requestQueue.showsNetworkActivityIndicatorWhenBusy = YES;
  
  NSManagedObjectModel *objectModel = [self managedObjectModel];
  RKManagedObjectStore* objectStore = [RKManagedObjectStore objectStoreWithStoreFilename:@"Kulturkiosk.sqlite" usingSeedDatabaseName:nil managedObjectModel:objectModel delegate:self];
  objectManager.objectStore = objectStore;
  
  [RKClient sharedClient].cachePolicy = RKRequestCachePolicyDefault;
  [RKClient sharedClient].requestCache.storagePolicy = RKRequestCacheStoragePolicyDisabled;
  objectStore.cacheStrategy = [RKFetchRequestManagedObjectCache new];
  
//  RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelDebug);
  RKLogConfigureByName("RestKit/CoreData", RKLogLevelOff);
  RKLogConfigureByName("RestKit", RKLogLevelOff);
  RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelOff);
  RKLogConfigureByName("RestKit/Network", RKLogLevelOff);
  
  /*!
   Map to a target object class -- just as you would for a non-persistent class. The entity is resolved
   for you using the Active Record pattern where the class name corresponds to the entity name within Core Data.
   */
  
  RKManagedObjectMapping* nodesMapping = [RKManagedObjectMapping mappingForClass:[Nodes class] inManagedObjectStore:objectStore];
  nodesMapping.primaryKeyAttribute = @"identifier";
  nodesMapping.rootKeyPath = @"data";

  [nodesMapping mapKeyPathsToAttributes:
   @"id", @"identifier",
   @"title", @"title",  
   @"parent", @"parent",
   @"position", @"position",
   @"node_id", @"node_id",
   @"page_id", @"page_id",
   @"record_id", @"record_id",
   @"item_id", @"item_id",
   @"type", @"type",
   nil];

  
  KKNodeObjectCache *cache = [[KKNodeObjectCache alloc] init];
  nodesMapping.objectStore.cacheStrategy = cache;
  
  RKManagedObjectMapping *resourcesMapping = [RKManagedObjectMapping mappingForClass:[Resources class] inManagedObjectStore:objectStore];
  resourcesMapping.primaryKeyAttribute = @"identifier";
  [resourcesMapping mapKeyPathsToAttributes:
   @"id", @"identifier",
   @"position", @"position",
   @"name", @"title",
   @"type", @"type",
   nil];
  
  RKManagedObjectMapping *descriptionsMapping = [RKManagedObjectMapping mappingForClass:[Description class] inManagedObjectStore:objectStore];
  [descriptionsMapping mapKeyPathsToAttributes:
   @"description", @"desc",
   @"title", @"title",
   @"language_code", @"language_code",
   @"language_id", @"language_id",
   @"language_title", @"language_title",
   nil];
  
  RKManagedObjectMapping *creditsMapping = [RKManagedObjectMapping mappingForClass:[Credit class] inManagedObjectStore:objectStore];
  [creditsMapping mapKeyPathsToAttributes:
   @"name", @"name",
   @"credit_type", @"credit_type",
   nil];
  
  RKManagedObjectMapping *filesMapping = [RKManagedObjectMapping mappingForClass:[Files class] inManagedObjectStore:objectStore];
  filesMapping.primaryKeyAttribute = @"url";
  [filesMapping mapKeyPathsToAttributes:@"url", @"url", @"type", @"type", nil];
  
  RKManagedObjectMapping *contentMapping = [RKManagedObjectMapping mappingForClass:[Content class] inManagedObjectStore:objectStore];
  contentMapping.primaryKeyAttribute = @"identifier";
  
  [contentMapping mapKeyPathsToAttributes:
   @"content_id", @"identifier",
   @"language_id", @"language_id",
   @"lanuage_title", @"lanuage_title",
   @"lanuage_code", @"lanuage_code",
   @"title", @"title",
   @"body", @"body",
   @"ingress", @"ingress",
   @"subtitle", @"subtitle",
   @"description", @"desc",
   @"updated_at", @"updated_at",
   @"created_at", @"created_at",
   @"position", @"position",
   nil];
  
  RKManagedObjectMapping *presentationsMapping = [RKManagedObjectMapping mappingForClass:[Presentation class] inManagedObjectStore:objectStore];
  presentationsMapping.primaryKeyAttribute = @"identifier";
  
  [presentationsMapping mapKeyPathsToAttributes:
   @"id", @"identifier",
   @"name", @"name",
   @"type", @"type",
   @"position", @"position",
   nil];
  
  // Settings
    RKManagedObjectMapping *settingsMapping = [RKManagedObjectMapping mappingForClass:[Setting class] inManagedObjectStore:objectStore];
  settingsMapping.primaryKeyAttribute = @"identifier";
  
  [settingsMapping mapKeyPathsToAttributes:
   @"id", @"identifier",
   @"value", @"value",
   nil];
  [objectManager.mappingProvider setMapping:settingsMapping forKeyPath:@"settings"];
  
  RKManagedObjectMapping *devicesMapping = [RKManagedObjectMapping mappingForClass:[Device class] inManagedObjectStore:objectStore];
  devicesMapping.primaryKeyAttribute = @"identifier";
    
  [devicesMapping mapKeyPathsToAttributes:
   @"id", @"identifier",
   @"name", @"name",
   @"reset_time",@"resetTime",
   @"idle_screen_active", @"idleScreenActive",
   @"idle_screen_title", @"idleScreenTitle",
   @"default_presentation", @"defaultPresentation",
   nil];
  
  RKManagedObjectMapping *recordsMapping = [RKManagedObjectMapping mappingForClass:[Record class] inManagedObjectStore:objectStore];
  recordsMapping.primaryKeyAttribute = @"identifier";
  
  [recordsMapping mapKeyPathsToAttributes:
   @"id", @"identifier",
   @"x", @"x",
   @"y", @"y",
   @"code", @"code",
   @"color", @"color",
   nil];
  [objectManager.mappingProvider setMapping:recordsMapping forKeyPath:@"data.records"];
  
  // Resources
  [resourcesMapping mapKeyPath:@"urls" toRelationship:@"files" withMapping:filesMapping];
  [resourcesMapping mapKeyPath:@"descriptions" toRelationship:@"descs" withMapping:descriptionsMapping];
  [resourcesMapping mapKeyPath:@"credits" toRelationship:@"credits" withMapping:creditsMapping];
  [objectManager.mappingProvider setMapping:resourcesMapping forKeyPath:@"resources"];

  // Nodes
  [nodesMapping mapKeyPath:@"contents" toRelationship:@"content" withMapping:contentMapping];
  [nodesMapping mapKeyPath:@"children" toRelationship:@"children" withMapping:nodesMapping];
  [nodesMapping mapKeyPath:@"resources" toRelationship:@"resources" withMapping:resourcesMapping];
  [objectManager.mappingProvider setMapping:nodesMapping forKeyPath:@"data"];
  
  // Presentations
  [presentationsMapping mapKeyPath:@"records" toRelationship:@"records" withMapping:recordsMapping];
  [presentationsMapping mapKeyPath:@"resources" toRelationship:@"resources" withMapping:resourcesMapping];
  [presentationsMapping mapKeyPath:@"contents" toRelationship:@"content" withMapping:contentMapping];
  [recordsMapping mapKeyPath: @"tree_node" toRelationship: @"node" withMapping:nodesMapping];
  [recordsMapping mapKeyPath: @"presentation" toRelationship:@"presentation" withMapping:presentationsMapping];
  [objectManager.mappingProvider setMapping:presentationsMapping forKeyPath:@"presentations"];

  // Devices
  [devicesMapping mapKeyPath:@"presentations" toRelationship:@"presentations" withMapping:presentationsMapping];
  [devicesMapping mapKeyPath:@"resources" toRelationship:@"resources" withMapping:resourcesMapping];
  [objectManager.mappingProvider setMapping:devicesMapping forKeyPath:@"device"];

  
  [RKObjectManager sharedManager].serializationMIMEType = RKMIMETypeJSON;
}

-(void) loadProjectFromNetwork
{
  NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
  NSInteger device   = [defaults integerForKey:@"device_id"];
  NSString *path = [NSString stringWithFormat:@"/device/%d/load_view", device];
  [[RKObjectManager sharedManager] loadObjectsAtResourcePath:path delegate:self];
}

/**
 * Sent when an object loaded failed to load the collection due to an error
 */
- (void)objectLoader:(RKObjectLoader*)objectLoader didFailWithError:(NSError*)error {
  if ([[[RKClient sharedClient] reachabilityObserver] isReachabilityDetermined] && [[RKClient sharedClient] isNetworkReachable]) {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Det har skjedd en feil!",@"") message:NSLocalizedString(@"Sjekk at du har satt riktig Utstillings-ID i innstillinger",@"") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
  }
}

- (void)managedObjectStore:(RKManagedObjectStore *)objectStore didFailToCreatePersistentStoreCoordinatorWithError:(NSError *)error
{
  NSLog(@"Deleting old db");
  [objectStore deletePersistentStore];
  [self setupRestKit];
  [self loadProjectFromNetwork];
}

- (void)objectLoaderDidLoadUnexpectedResponse:(RKObjectLoader *)objectLoader
{
  
}

/**
 Invoked when the object loader has finished loading
 */
- (void)objectLoaderDidFinishLoading:(RKObjectLoader*)objectLoader
{
  KKAssetsManager *assetsManager = [KKAssetsManager sharedManager];
  
  for (id obj in [Nodes allObjects]) {
    if ([[obj class] isSubclassOfClass:[Nodes class]]) {
      // Load the assets
      [assetsManager loadAssetsForObject:(Nodes*)obj];
    }
  }
  
  for (id obj in [Device allObjects]) {
    if ([[obj class] isSubclassOfClass:[Device class]]) {
        // Load the assets
      [assetsManager loadAssetsForObject:(Device*)obj];
    }
  }
  
  for (id obj in [Presentation allObjects]) {
    if ([[obj class] isSubclassOfClass:[Presentation class]]) {
        // Load the assets
      [assetsManager loadAssetsForObject:(Presentation*)obj];
    }
  }
  
  KKLoadingViewController *vc=(KKLoadingViewController*) self.window.rootViewController;
  [[NSNotificationCenter defaultCenter] addObserver:vc selector:@selector(loadedFile:) name:@"loadedFile" object:nil];
  // The KKAssetsManager is responsible for triggering "loadingFinished" when it finishes
  // downloading assets. If nothing is to be downloaded, we have to trigger it here to make
  // the app launch.
  if([assetsManager.queue count] == 0) {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadingFinished" object:nil];
  }
}


#pragma mark - Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext
{
  if (__managedObjectContext != nil)
  {
    return __managedObjectContext;
  }
  
  NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
  if (coordinator != nil)
  {
    __managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [__managedObjectContext setPersistentStoreCoordinator:coordinator];
  }
  return __managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel
{
  if (__managedObjectModel != nil)
  {
    return __managedObjectModel;
  }
  NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
  __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
  return __managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
  if (__persistentStoreCoordinator != nil)
  {
    return __persistentStoreCoordinator;
  }
  
  NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Kulturkiosk.sqlite"];
  
  NSError *error = nil;
  
  NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                           [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                           [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
  
  __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
  if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error])
  {

    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    //abort();
  }
  
  return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory
{
  return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


@end
