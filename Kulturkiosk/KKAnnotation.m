//
//  KKAnnotation.m
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 04.06.13.
//
//

#import "KKAnnotation.h"

@implementation KKAnnotation


+ (id)annotationWithPoint:(CGPoint)point{
  return [[[self class] alloc] initWithPoint:point];
}

- (id)initWithPoint:(CGPoint)point{
  self = [super init];
  if (self) {
    self.point    = point;
    self.color    = [UIColor blackColor];
    self.title    = nil;
    self.subtitle = nil;
    self.contexts = nil;
    self.selected = NO;
  }
  return self;
}

- (NSString *)debugDescription
{
  return [NSString stringWithFormat:@"%@ - %@", self.title, NSStringFromCGPoint(self.point)];
}
@end
