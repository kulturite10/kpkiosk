// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Description.m instead.

#import "_Description.h"

const struct DescriptionAttributes DescriptionAttributes = {
	.desc = @"desc",
	.language_code = @"language_code",
	.language_id = @"language_id",
	.language_title = @"language_title",
	.title = @"title",
};

const struct DescriptionRelationships DescriptionRelationships = {
	.resource = @"resource",
};

const struct DescriptionFetchedProperties DescriptionFetchedProperties = {
};

@implementation DescriptionID
@end

@implementation _Description

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Description" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Description";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Description" inManagedObjectContext:moc_];
}

- (DescriptionID*)objectID {
	return (DescriptionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"language_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"language_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic desc;






@dynamic language_code;






@dynamic language_id;



- (int16_t)language_idValue {
	NSNumber *result = [self language_id];
	return [result shortValue];
}

- (void)setLanguage_idValue:(int16_t)value_ {
	[self setLanguage_id:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveLanguage_idValue {
	NSNumber *result = [self primitiveLanguage_id];
	return [result shortValue];
}

- (void)setPrimitiveLanguage_idValue:(int16_t)value_ {
	[self setPrimitiveLanguage_id:[NSNumber numberWithShort:value_]];
}





@dynamic language_title;






@dynamic title;






@dynamic resource;

	






@end
