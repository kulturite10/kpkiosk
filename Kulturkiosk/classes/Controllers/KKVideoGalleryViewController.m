
  //
//  KKVideoGalleryViewController.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 29.05.12.
//  Copyright (c) 2012 Nordaaker Ltd. All rights reserved.
//

#import "KKVideoGalleryViewController.h"
#import "Resources.h"
#import "Files.h"
#import "Credit.h"


@interface KKVideoGalleryViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageContainer;
@property (weak, nonatomic) IBOutlet UIScrollView *thumbnailContainer;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoTitle;
@property (weak, nonatomic) IBOutlet UIView *videoWrapper;

@end

@implementation KKVideoGalleryViewController

-(void) dealloc
{
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  UIAppDelegate.shouldRegisterTouch = YES;
}

-(void) viewDidLoad
{
  [super viewDidLoad];
  [self.titleLabel setText:nil];
  [self.videoTitle setText:nil];
  [self.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:36]];
  [self.videoTitle setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:18]];
  
  UIAppDelegate.shouldRegisterTouch = NO;
  
  self.moviePlayer = [[MPMoviePlayerController alloc] init];
  
  // Do any additional setup after loading the view
  NSMutableArray *tempArray = [[NSMutableArray alloc] init];
  for (Resources *res in self.resources) {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    NSString *descString = @"", *creditString = @"";
    NSString *titleString=@"";
    
    KKLanguageType current = [KKLanguage currentLanguage];
    NSArray *descriptions = [res descriptionsForLanguage:current];
    
    for(Description *d in descriptions) {
      if([d.desc length]) {
        descString = [descString stringByAppendingFormat:@"%@\n", d.desc];
      }
      if([d.title length]) {
        titleString = [titleString stringByAppendingFormat:@"%@\n", d.title];
      }

    }
    
    for(Credit *c in res.credits) {
      NSString *credit = [c localizedCredit];
      if(credit)
        creditString = [creditString stringByAppendingFormat:@"%@\n", credit];
    }
    
    [dict setObject:creditString forKey:@"credit"];
    [dict setObject:descString   forKey:@"description"];
    [dict setObject:titleString   forKey:@"title"];

    [tempArray addObject:dict];
    
  }
  
  if([tempArray count] < 1) {
    return;
  }
  
  self.resourceDetails = [NSArray arrayWithArray:tempArray];
  
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopTimer) name:MPMoviePlayerDidEnterFullscreenNotification object:self.moviePlayer];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startTimer) name:MPMoviePlayerPlaybackDidFinishNotification object:self.moviePlayer];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopTimer) name:MPMoviePlayerPlaybackStateDidChangeNotification object:self.moviePlayer];
    
  [self.videoWrapper addSubview:self.moviePlayer.view];
  
  
  [self setupCreditView];
  
  self.videoTitle.text = [(NSDictionary*)[tempArray objectAtIndex:0] objectForKey:@"title"];

}

-(void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  
  if (!self.moviePlayer.contentURL) {
    NSInteger index = [self indexOfItemView:0];
    Resources *r = [self.resources objectAtIndex:(NSUInteger)index];
    
    Files *video = [[r.files allObjects] objectAtIndex:0];
    NSURL *url = [NSURL fileURLWithPath:video.localUrl];
    [self.moviePlayer setContentURL:url];
    
    __block long long delayInNanoSeconds = (long long)(1.0 * NSEC_PER_SEC);
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInNanoSeconds);
    dispatch_after(popTime, dispatch_get_main_queue(), ^{
      [self selectThumb:0];
      [self.moviePlayer play];
    });

  }

}


- (void) viewDidLayoutSubviews
{
  [super viewDidLayoutSubviews];
  
  CGRect videoFrame = self.videoWrapper.frame;
  videoFrame.origin.x = videoFrame.origin.y = 0;
  self.moviePlayer.view.frame = videoFrame;
  
  CGSize thumbSize = CGSizeMake(300, 200);
  NSUInteger offset = 10, horizontalMargin = 10, verticalMargin = 10;
  
  for(NSUInteger i = 0; i < [self.resources count]; i++) {
    Resources *resource = [self.resources objectAtIndex:i];
    
    Files *video = [[resource.files allObjects] objectAtIndex:0];

    UIImage *thumb = [video thumbnailWithSize:&thumbSize];

    UIImageView *thumbnail = [[UIImageView alloc] initWithImage:thumb];

    CGRect thumbFrame = CGRectMake(offset, verticalMargin/2, thumbSize.width, thumbSize.height);
    [thumbnail setFrame:thumbFrame];
    offset += (thumbSize.width + horizontalMargin);
    CGRect  titleFrame=CGRectMake(0, ((thumbSize.height/5)*4), thumbSize.width, thumbSize.height/5);
    UILabel *titleLabel = [[UILabel alloc] initWithFrame: titleFrame];
    titleLabel.text=[(NSDictionary*)[self.resourceDetails objectAtIndex:i] objectForKey:@"title"];
    titleLabel.backgroundColor=[UIColor colorWithRed:0.50 green:0.50 blue:0.50 alpha:0.60];
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    [titleLabel setFont: [UIFont fontWithName:@"MyriadPro-Bold" size:18]];
    [thumbnail addSubview:titleLabel];
    [thumbnail setContentMode:UIViewContentModeScaleAspectFill];
    [thumbnail setAutoresizesSubviews:YES];
    [thumbnail setClipsToBounds:YES];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
    thumbnail.tag = i;
    [thumbnail addGestureRecognizer:tapGesture];
    [thumbnail setUserInteractionEnabled:YES];
    
    [self.thumbnailContainer addSubview:thumbnail];
    [self.thumbViews addObject:thumbnail];
  }
  
  // Set the scroll view content size to fit all added thumbs (accumulated 'offset' variable), plus their height + vertical margin
  [self.thumbnailContainer setContentSize:CGSizeMake(offset, thumbSize.height + verticalMargin)];
  
  [self.titleLabel setText:self.title];
  [self.spinner stopAnimating];
  

}

-(void) setupCreditView
{
  
  NSArray *array = [[UINib nibWithNibName:@"KKResourceCreditView" bundle:nil] instantiateWithOwner:self options:nil];
  for(id obj in array) {
    if([obj class] == [KKResourceCreditView class]) {
      self.infoView = (KKResourceCreditView*)obj;
    }
  }
  
  if(self.infoView) {
    CGRect creditFrame = self.infoView.frame;
    // Hidden initially
    creditFrame.origin.x = -creditFrame.size.width;
    self.infoView.frame = creditFrame;
    [self.view addSubview:self.infoView];
  }
}

- (IBAction)goBack:(id)sender {
  
  self.videoWrapper = nil;
  [self.moviePlayer stop];
  [self setMoviePlayer:nil];
  [self.view.window makeKeyAndVisible];
  [self setResources:nil];
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didTap:(UITapGestureRecognizer *)tapGesture
{
  [super didTap:tapGesture];
  [self.moviePlayer stop];
  [UIView animateWithDuration:0.3 animations:^{
    [self.thumbnailContainer scrollRectToVisible:tapGesture.view.frame animated:NO];
  } completion:^(BOOL finished) {
    NSInteger index = [self indexOfItemView:tapGesture.view];
    self.videoTitle.text = [(NSDictionary*)[self.resourceDetails objectAtIndex:index] objectForKey:@"title"];

    Resources *r = [self.resources objectAtIndex:(NSUInteger)index];
    
    Files *video = [[r.files allObjects] objectAtIndex:0];
    NSURL *url = [NSURL fileURLWithPath:video.localUrl];
    [self.moviePlayer setContentURL:url];
   
    [self.moviePlayer play];
    
  }];
}

- (void)stopTimer
{
  if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
    [UIAppDelegate stopTimer];
  }else{
    [self startTimer];
  }
}

- (void)startTimer
{
  [UIAppDelegate startTimer];
}



@end
