// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Record.h instead.

#import <CoreData/CoreData.h>


extern const struct RecordAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *color;
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *x;
	__unsafe_unretained NSString *y;
} RecordAttributes;

extern const struct RecordRelationships {
	__unsafe_unretained NSString *node;
	__unsafe_unretained NSString *presentation;
} RecordRelationships;

extern const struct RecordFetchedProperties {
} RecordFetchedProperties;

@class Nodes;
@class Presentation;







@interface RecordID : NSManagedObjectID {}
@end

@interface _Record : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (RecordID*)objectID;





@property (nonatomic, strong) NSString* code;



//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* color;



//- (BOOL)validateColor:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* identifier;



@property int32_t identifierValue;
- (int32_t)identifierValue;
- (void)setIdentifierValue:(int32_t)value_;

//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* x;



@property int32_t xValue;
- (int32_t)xValue;
- (void)setXValue:(int32_t)value_;

//- (BOOL)validateX:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* y;



@property int32_t yValue;
- (int32_t)yValue;
- (void)setYValue:(int32_t)value_;

//- (BOOL)validateY:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Nodes *node;

//- (BOOL)validateNode:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) Presentation *presentation;

//- (BOOL)validatePresentation:(id*)value_ error:(NSError**)error_;





@end

@interface _Record (CoreDataGeneratedAccessors)

@end

@interface _Record (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveCode;
- (void)setPrimitiveCode:(NSString*)value;




- (NSString*)primitiveColor;
- (void)setPrimitiveColor:(NSString*)value;




- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int32_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int32_t)value_;




- (NSNumber*)primitiveX;
- (void)setPrimitiveX:(NSNumber*)value;

- (int32_t)primitiveXValue;
- (void)setPrimitiveXValue:(int32_t)value_;




- (NSNumber*)primitiveY;
- (void)setPrimitiveY:(NSNumber*)value;

- (int32_t)primitiveYValue;
- (void)setPrimitiveYValue:(int32_t)value_;





- (Nodes*)primitiveNode;
- (void)setPrimitiveNode:(Nodes*)value;



- (Presentation*)primitivePresentation;
- (void)setPrimitivePresentation:(Presentation*)value;


@end
