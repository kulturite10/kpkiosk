// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Record.m instead.

#import "_Record.h"

const struct RecordAttributes RecordAttributes = {
	.code = @"code",
	.color = @"color",
	.identifier = @"identifier",
	.x = @"x",
	.y = @"y",
};

const struct RecordRelationships RecordRelationships = {
	.node = @"node",
	.presentation = @"presentation",
};

const struct RecordFetchedProperties RecordFetchedProperties = {
};

@implementation RecordID
@end

@implementation _Record

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Record" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Record";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Record" inManagedObjectContext:moc_];
}

- (RecordID*)objectID {
	return (RecordID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"xValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"x"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"yValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"y"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic code;






@dynamic color;






@dynamic identifier;



- (int32_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result intValue];
}

- (void)setIdentifierValue:(int32_t)value_ {
	[self setIdentifier:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result intValue];
}

- (void)setPrimitiveIdentifierValue:(int32_t)value_ {
	[self setPrimitiveIdentifier:[NSNumber numberWithInt:value_]];
}





@dynamic x;



- (int32_t)xValue {
	NSNumber *result = [self x];
	return [result intValue];
}

- (void)setXValue:(int32_t)value_ {
	[self setX:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveXValue {
	NSNumber *result = [self primitiveX];
	return [result intValue];
}

- (void)setPrimitiveXValue:(int32_t)value_ {
	[self setPrimitiveX:[NSNumber numberWithInt:value_]];
}





@dynamic y;



- (int32_t)yValue {
	NSNumber *result = [self y];
	return [result intValue];
}

- (void)setYValue:(int32_t)value_ {
	[self setY:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveYValue {
	NSNumber *result = [self primitiveY];
	return [result intValue];
}

- (void)setPrimitiveYValue:(int32_t)value_ {
	[self setPrimitiveY:[NSNumber numberWithInt:value_]];
}





@dynamic node;

	

@dynamic presentation;

	






@end
