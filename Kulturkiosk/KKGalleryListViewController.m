//
//  KKGalleryListViewController.m
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 04.11.13.
//
//

#import "KKGalleryListViewController.h"
#import "KKIndexCollectionViewCell.h"

@interface KKGalleryListViewController ()

@end

@implementation KKGalleryListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  Content *content= [self.presentation contentForLanguage: [KKLanguage currentLanguage]];
  [self.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:36]];
  self.titleLabel.text=content ? content.title : @"";
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
  Record *record = [self.sortedRecords objectAtIndex:indexPath.row];
  if([self.presentation.type isEqualToString: @"video_gallery"]) {
    [self performSegueWithIdentifier: @"presentVideo" sender:record.node];
  }
  else {
    [self performSegueWithIdentifier: @"presentImage" sender:record.node];
  }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  KKIndexCollectionViewCell* cell=(KKIndexCollectionViewCell*)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
  if(![self.presentation.type isEqualToString: @"video_gallery"]) {
    cell.playButton.hidden=YES;
  }
  return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  id vc = segue.destinationViewController;
  if([[vc class] isSubclassOfClass: [KKPresentationViewController class]]) {
    [vc setNode: sender];
  }

}
@end
