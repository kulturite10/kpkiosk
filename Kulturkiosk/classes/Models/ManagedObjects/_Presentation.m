// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Presentation.m instead.

#import "_Presentation.h"

const struct PresentationAttributes PresentationAttributes = {
	.identifier = @"identifier",
	.name = @"name",
	.position = @"position",
	.type = @"type",
};

const struct PresentationRelationships PresentationRelationships = {
	.content = @"content",
	.devices = @"devices",
	.records = @"records",
	.resources = @"resources",
};

const struct PresentationFetchedProperties PresentationFetchedProperties = {
};

@implementation PresentationID
@end

@implementation _Presentation

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Presentation" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Presentation";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Presentation" inManagedObjectContext:moc_];
}

- (PresentationID*)objectID {
	return (PresentationID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"positionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"position"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic identifier;



- (int16_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result shortValue];
}

- (void)setIdentifierValue:(int16_t)value_ {
	[self setIdentifier:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveIdentifierValue:(int16_t)value_ {
	[self setPrimitiveIdentifier:[NSNumber numberWithShort:value_]];
}





@dynamic name;






@dynamic position;



- (int16_t)positionValue {
	NSNumber *result = [self position];
	return [result shortValue];
}

- (void)setPositionValue:(int16_t)value_ {
	[self setPosition:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePositionValue {
	NSNumber *result = [self primitivePosition];
	return [result shortValue];
}

- (void)setPrimitivePositionValue:(int16_t)value_ {
	[self setPrimitivePosition:[NSNumber numberWithShort:value_]];
}





@dynamic type;






@dynamic content;

	
- (NSMutableSet*)contentSet {
	[self willAccessValueForKey:@"content"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"content"];
  
	[self didAccessValueForKey:@"content"];
	return result;
}
	

@dynamic devices;

	
- (NSMutableSet*)devicesSet {
	[self willAccessValueForKey:@"devices"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"devices"];
  
	[self didAccessValueForKey:@"devices"];
	return result;
}
	

@dynamic records;

	
- (NSMutableSet*)recordsSet {
	[self willAccessValueForKey:@"records"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"records"];
  
	[self didAccessValueForKey:@"records"];
	return result;
}
	

@dynamic resources;

	
- (NSMutableSet*)resourcesSet {
	[self willAccessValueForKey:@"resources"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"resources"];
  
	[self didAccessValueForKey:@"resources"];
	return result;
}
	






@end
