//
//  KKItemNavigationViewController.h
//  Kulturkiosk
//
//  Created by Anders Wiik on 21.05.12.
//  Copyright (c) 2012 Nordaaker Ltd.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KKNavBarController.h"

@class KKTimeLineViewController;

@interface KKItemNavigationViewController : UIViewController <KKNavBarControllerDelegate>

@property (strong, nonatomic) KKNavBarController *navBarController;
@property (strong, nonatomic) KKTimeLineViewController *timeLineViewController;

@end
