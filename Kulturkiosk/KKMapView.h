//
//  KKMapView.h
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 04.06.13.
//
//


@class KKAnnotation, KKCalloutView;

@protocol KKMapViewDelegate <NSObject>
@required
- (void) willSelectAnnotation:(KKAnnotation*)annotation;
- (void) didSelectAnnotation:(KKAnnotation *)annotation;
- (void) didDeselectAnnotation:(KKAnnotation *)annotation;
@end

@interface KKMapView : UIScrollView <UIScrollViewDelegate>

@property (nonatomic, weak) NSObject <KKMapViewDelegate> *mapViewdelegate;
@property (nonatomic, assign) CGSize          orignalSize;
@property (nonatomic, strong) NSMutableArray *annotationViews;
@property (nonatomic, assign) CGFloat annotationOffset;

- (void)displayMap:(UIImage *)map;
- (void)addAnnotation:(KKAnnotation *)annotation animated:(BOOL)animate;
- (void)addAnnotations:(NSArray *)annotations animated:(BOOL)animate;
- (void)centreOnPoint:(CGPoint)point animated:(BOOL)animate;
- (void)removeAnnotation:(KKAnnotation *)annotation;
- (CGPoint)zoomRelativePoint:(CGPoint)point;
-(void)removeAnnotations;
- (void)showCallOut:(UITapGestureRecognizer*)sender;

@end



