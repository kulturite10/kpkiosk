// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Resources.m instead.

#import "_Resources.h"

const struct ResourcesAttributes ResourcesAttributes = {
	.identifier = @"identifier",
	.page_id = @"page_id",
	.position = @"position",
	.title = @"title",
	.type = @"type",
};

const struct ResourcesRelationships ResourcesRelationships = {
	.credits = @"credits",
	.descs = @"descs",
	.device = @"device",
	.files = @"files",
	.node = @"node",
	.presentation = @"presentation",
};

const struct ResourcesFetchedProperties ResourcesFetchedProperties = {
};

@implementation ResourcesID
@end

@implementation _Resources

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Resources" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Resources";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Resources" inManagedObjectContext:moc_];
}

- (ResourcesID*)objectID {
	return (ResourcesID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"page_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"page_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"positionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"position"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic identifier;



- (int32_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result intValue];
}

- (void)setIdentifierValue:(int32_t)value_ {
	[self setIdentifier:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result intValue];
}

- (void)setPrimitiveIdentifierValue:(int32_t)value_ {
	[self setPrimitiveIdentifier:[NSNumber numberWithInt:value_]];
}





@dynamic page_id;



- (int16_t)page_idValue {
	NSNumber *result = [self page_id];
	return [result shortValue];
}

- (void)setPage_idValue:(int16_t)value_ {
	[self setPage_id:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePage_idValue {
	NSNumber *result = [self primitivePage_id];
	return [result shortValue];
}

- (void)setPrimitivePage_idValue:(int16_t)value_ {
	[self setPrimitivePage_id:[NSNumber numberWithShort:value_]];
}





@dynamic position;



- (int16_t)positionValue {
	NSNumber *result = [self position];
	return [result shortValue];
}

- (void)setPositionValue:(int16_t)value_ {
	[self setPosition:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePositionValue {
	NSNumber *result = [self primitivePosition];
	return [result shortValue];
}

- (void)setPrimitivePositionValue:(int16_t)value_ {
	[self setPrimitivePosition:[NSNumber numberWithShort:value_]];
}





@dynamic title;






@dynamic type;






@dynamic credits;

	
- (NSMutableSet*)creditsSet {
	[self willAccessValueForKey:@"credits"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"credits"];
  
	[self didAccessValueForKey:@"credits"];
	return result;
}
	

@dynamic descs;

	
- (NSMutableSet*)descsSet {
	[self willAccessValueForKey:@"descs"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"descs"];
  
	[self didAccessValueForKey:@"descs"];
	return result;
}
	

@dynamic device;

	

@dynamic files;

	
- (NSMutableSet*)filesSet {
	[self willAccessValueForKey:@"files"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"files"];
  
	[self didAccessValueForKey:@"files"];
	return result;
}
	

@dynamic node;

	

@dynamic presentation;

	






@end
