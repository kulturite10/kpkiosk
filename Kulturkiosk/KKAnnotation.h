//
//  KKAnnotation.h
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 04.06.13.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIColor.h>

@interface KKAnnotation : NSObject

@property (nonatomic, assign) CGPoint    point;
@property (nonatomic, assign) UIColor   *color;
@property (nonatomic, copy)   NSString  *title;
@property (nonatomic, copy)   NSString  *subtitle;
@property (nonatomic, strong) NSArray   *contexts;
@property (nonatomic, copy)   UIColor   *backgroundColor;
@property (nonatomic, strong) UIImage   *thumbnail;
@property (nonatomic, copy)   NSString  *ingress;
@property (nonatomic, copy)   NSString  *code;
@property (nonatomic, strong) NSNumber  *tag;
@property (nonatomic, assign) BOOL       selected;

+ (id)annotationWithPoint:(CGPoint)point;
- (id)initWithPoint:(CGPoint)point;

@end

