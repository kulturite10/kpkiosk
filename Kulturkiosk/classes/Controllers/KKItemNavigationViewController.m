//
//  KKItemNavigationViewController.m
//  Kulturkiosk
//
//  Created by Anders Wiik on 21.05.12.
//  Copyright (c) 2012 Nordaaker Ltd.. All rights reserved.
//

#import "KKItemNavigationViewController.h"
#import "KKTimeLineViewController.h"

@interface KKItemNavigationViewController() {
  BOOL backButtonState;
}

@end
@implementation KKItemNavigationViewController

@synthesize navBarController, timeLineViewController;

-(void) goBack
{
  [self toggleNavigation];
}

- (void) toggleNavigation
{
  [self.navBarController setBackEnabled:backButtonState];
  [self.navBarController.view removeFromSuperview];
  [self.timeLineViewController.view addSubview:self.navBarController.view];
  [self dismissViewControllerAnimated:YES completion:^{
    self.navBarController.navButton.imageView.image = [UIImage imageNamed:@"oversikt-icon"];
    self.navBarController.delegate = self.timeLineViewController;
  }];
}

-(void) viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  backButtonState = [self.navBarController backEnabled];
  [self.navBarController setBackEnabled:YES];
}

-(void) viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  self.navBarController.navButton.imageView.image = [UIImage imageNamed:@"oversikt-icon-active"];
}

@end
