//
//  KKLanguage.h
//  Kulturkiosk
//
//  Created by Rune Botten on 16.10.12.
//
//

#import <Foundation/Foundation.h>

typedef enum KKLanguageType
{
  KKLanguageTypeEnglish   = 1,
  KKLanguageTypeFrench    = 2,
  KKLanguageTypeGerman    = 3,
  KKLanguageTypeSwedish   = 4,
  KKLanguageTypeSpanish   = 5,
  KKLanguageTypeNorwegian = 6,
} KKLanguageType;

@interface KKLanguage : NSObject
+(NSArray*) availableLanguages;
+(KKLanguageType) currentLanguage;
+(BOOL) setLanguage:(KKLanguageType) newLanguage;
+(NSString*) localizedString:(NSString*) key;
@end
