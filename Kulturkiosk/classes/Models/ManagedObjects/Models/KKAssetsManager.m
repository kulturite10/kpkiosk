//
//  KKAssetsManager.m
//  Kulturkiosk
//
//  Created by Martin Jensen on 18.07.12.
//
//

#import "KKAssetsManager.h"
#import "KKAppDelegate.h"

@interface KKAssetsManager ()

@property (nonatomic, assign) int requestCount;

@end

@implementation KKAssetsManager
@synthesize requestCount;

+ (id)sharedManager
{
  static dispatch_once_t pred = 0;
  __strong static id _sharedManager = nil;
  dispatch_once(&pred, ^{
    _sharedManager = [[self alloc] init]; // or some other init method
  });
  return _sharedManager;
}

-(id)init
{
  self = [super init];
  if (self) {
    self.queue = [[RKRequestQueue alloc] init];
    self.queue.delegate = self;
    self.queue.concurrentRequestsLimit = 5;
    self.queue.showsNetworkActivityIndicatorWhenBusy = YES;
  }
  return self;
}

/*
 * Queues downloading assets for nodes
 */
-(void)loadAssetsForNode:(Nodes*)node
{
  // Get the resource-entity for the node, and load every file as request
  
  NSArray *files = [node files];
  for (Files *file in files) {
    
    // Download files we have not downloaded.
    if(!file.localUrl || ![[NSFileManager defaultManager] fileExistsAtPath:file.localUrl])
    {
      // Request for files here
      //Ensure url is properly encoded
      
      file.url = [file.url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
      RKRequest *request = [[RKClient sharedClient] get:file.url delegate:self];
      request.userData = file;
      if (![self.queue containsRequest:request]) {
        [self.queue addRequest:request];
      }
    }    
  }
}

-(void) request:(RKRequest *)request didFailLoadWithError:(NSError *)error
{
  Files *file = (Files*)request.userData;
  Resources *resource = file.resource;

  NSLog(@"Failed to download %@ in %@", file, resource);
}

-(void)requestQueue:(RKRequestQueue *)queue didLoadResponse:(RKResponse *)response
{
  Files *file = (Files*)response.request.userData;
  Resources *resource = file.resource;

  NSURL *fileURL = [NSURL URLWithString:file.url];
  NSString* fileName = [NSString stringWithFormat:@"%@-%@-%@", resource.identifier, file.type, [fileURL lastPathComponent]];
  
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
  NSString *dir = [paths objectAtIndex:0]; // Get caches folder
  NSString *cacheDir = [dir stringByAppendingPathComponent:@"DownloadedMedia"];
  
  // TODO: Error checking
  if (![[NSFileManager defaultManager] fileExistsAtPath:cacheDir])
    [[NSFileManager defaultManager] createDirectoryAtPath:cacheDir withIntermediateDirectories:NO attributes:nil error:nil]; //Create folder
  
  fileName =[cacheDir stringByAppendingPathComponent:fileName isDirectory:NO];
  //NSLog(@"Writing file to disk %@", fileName);
  [response.body writeToFile:fileName atomically:YES];
  
  // File is created on disk, store this new location in file.localUrl
  file.localUrl = fileName;
  
  // The damn queue stops at 1 and does not trigger the delegate method
  if([self.queue count] == 1) {
    [self requestQueueDidFinishLoading:self.queue];
  }
}

- (void)requestQueue:(RKRequestQueue *)queue didFailRequest:(RKRequest *)request withError:(NSError *)error
{
  NSLog(@"%@", [error localizedDescription]);
  
}

- (void)requestQueueDidFinishLoading:(RKRequestQueue *)queue
{
  [[NSNotificationCenter defaultCenter] postNotificationName:@"loadingFinished" object:nil];
}

@end
